$(".option").click(function (e) {
    $(".bottom .selected").removeClass("selected");
    $(".frame .selected").removeClass("selected");

    let id = $(e.currentTarget).attr("data-id");

    $(this).addClass("selected");
    $("#"+ id).addClass("selected");
});