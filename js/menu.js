$("#menu-toggle").click(function () {
    $(".menu").toggleClass("opened");
    $(".dimmer").toggleClass("active");
});

$(".dimmer").click(function () {
    $(".menu").removeClass("opened");
    $(".dimmer").removeClass("active");
});

$(".item").click(function (e) {
    let target = $(e.currentTarget).attr("data-url");

    $("#menu-toggle").click();
    setTimeout(function () {
        document.location.href = target;
    },200);
});